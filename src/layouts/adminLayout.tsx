import React, { useEffect, useState } from "react";
import { Navigate, Outlet, useLocation } from "react-router-dom";
import { AUTH, ERROR } from "../routes/routes";
import { useAppSelector } from "../store/hooks";
import ROLES from "../config/roles";
import AdminHeader from "../components/admin/header";
import AdminSideNav from "../components/admin/sidenav";

const AdminLayout: React.FC<{
  isSuperAdmin: boolean;
}> = ({ isSuperAdmin = false }) => {
  const [navToggled, setNavToggled] = useState(false);

  const { user, isAuthenticated } = useAppSelector((state) => state.user);
  const location = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location?.pathname]);

  if (
    (isSuperAdmin && user?.role !== ROLES.SUPER_ADMIN) ||
    (!isSuperAdmin && user?.role !== ROLES.ADMIN)
  ) {
    return <Navigate to={isAuthenticated ? ERROR.ERROR_403 : AUTH.BASE_PATH} />;
  }

  return (
    <>
      <AdminHeader />
      <div id="layoutSidenav">
        <AdminSideNav />
        <div id="layoutSidenav_content">
          <Outlet />
        </div>
      </div>
    </>
  );
};

export default AdminLayout;
