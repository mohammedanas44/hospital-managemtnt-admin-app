import React from "react";
import useLoginState from "./useLoginState";

const LoginPage: React.FC = () => {
  const { formik, debouncedSubmit } = useLoginState();
  return (
    <div>
      <section className="new-patient-wrap">
        <div className="container" style={{ marginTop: "200px" }}>
          <div className="d-flex justify-content-center">
            <h2>Admin Login</h2>
          </div>

          <div className="row-wrap">
            <div className="column">
              <div>
                <input
                  id="email"
                  className="input name"
                  type="email"
                  placeholder="Email"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.errors.email && formik.touched.email && (
                  <p className={"error-info"}>{formik.errors.email}</p>
                )}
              </div>
            </div>
          </div>
          <div className="row-wrap">
            <div className="column">
              <div>
                <input
                  id="password"
                  className="input name"
                  type="password"
                  placeholder="Password"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.errors.password && formik.touched.password && (
                  <p className={"error-info"}>{formik.errors.password}</p>
                )}
              </div>
            </div>
          </div>
          <div className="row-wrap row-gender">
            <span className="search-btn">
              <button
                className="btn more active"
                onClick={() => debouncedSubmit(formik.values)}>
                <b style={{ color: "#ffff" }}>Login</b>
              </button>
            </span>
          </div>
        </div>
      </section>
    </div>
  );
};

export default LoginPage;
