import React from "react";
import { useEffect, useState } from "react";
import { debounce } from "lodash";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useAppDispatch } from "../../../store/hooks";
import { useAppSelector } from "../../../store/hooks";
import { login } from "../../../store/slices/user";
interface Values {
  email: string;
  password: string;
}

const LoginSchema = Yup.object().shape({
  email: Yup.string().required("Email is required"),
  password: Yup.string().required("Password is required"),
});
const useLoginState = () => {
  const dispatch = useAppDispatch();

  const onSubmit = async (values: Values) => {
    const params = {
      email: values.email,
      password: values.password,
    };
    dispatch(login(params));
  };
  const debouncedSubmit = debounce(onSubmit, 300);
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: LoginSchema,
    onSubmit: (values) => debouncedSubmit(values),
  });
  return { formik, debouncedSubmit };
};

export default useLoginState;
