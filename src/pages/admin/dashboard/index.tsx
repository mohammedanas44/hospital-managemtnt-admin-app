import React from "react";

const DashboardPage: React.FC = () => {
  return <div>
  <div className="patient-list">
      <a className="back-btn" href=""></a>
  </div>
  <section className="new-patient-wrap profile-outer-wrap">
      {/* <Sidebar user={location.state}/> */}
      <div className="inner-body">
          <h2>Dashboard </h2>
          <div className="input-wraper">
          </div>
          <div className="search-list billing-wrap">
              <ul>
                  <li>
                      <div>
                          <h5>Hospitals</h5>
                      </div>
                      <div>
                          <h5>5</h5>
                      </div>
                  </li>
                  <li>
                      <div>
                          <h5>Feature</h5>
                      </div>
                      <div>
                          <h5>5</h5>
                      </div>
                  </li>
                  <li>
                      <div>
                          <h5>Role</h5>
                      </div>
                      <div>
                          <h5>7</h5>
                      </div>
                  </li>
                  <li>
                      <div>
                          <h5>Department</h5>
                      </div>
                      <div>
                          <h5>25</h5>
                      </div>
                  </li>
                  <li>
                      <div>
                          <h5>Service</h5>
                      </div>
                      <div>
                          <h5>3</h5>
                      </div>
                  </li>
              </ul>
          </div>
      </div>
  </section>
</div>;
};

export default DashboardPage;
