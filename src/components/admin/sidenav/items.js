const sideNavItems = [
  {
    link: "dashboard",
    // icon: "fa-cube",
    label: "Dashboard",
    enabled: true,
    key: "dashboard",
  },
  {
    link: "department",
    // icon: "fa-cube",
    label: "Department",
    enabled: true,
    key: "department",
  },
  {
    link: "service",
    // icon: "fa-cube",
    label: "Service",
    enabled: true,
    key: "service",
  },
  {
    link: "features",
    // icon: "fa-cube",
    label: "Features",
    enabled: true,
    key: "features",
  },
  {
    link: "roles",
    // icon: "fa-cube",
    label: "Roles",
    enabled: true,
    key: "roles",
  },
  {
    link: "hospital",
    // icon: "fa-cube",
    label: "Hospital",
    enabled: true,
    key: "hospital",
  },
  {
    link: "profile",
    // icon: "fa-cube",
    label: "Profile",
    enabled: true,
    key: "profile",
  },
];

export default sideNavItems;
