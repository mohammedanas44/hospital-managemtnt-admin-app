import React from "react";
import { Link, useLocation } from "react-router-dom";
import sideNavItems from "./items";

const AdminSideNav: React.FC = () => {
  const location = useLocation();

  return (
    <div id="layoutSidenav_nav">
      <nav
        className="sb-sidenav accordion sb-sidenav-light"
        id="sidenavAccordion">
        <div className="sb-sidenav-menu">
          <div className="nav profile-details">
            {sideNavItems && sideNavItems.length > 0
              ? sideNavItems.map((item, i) => {
                  if (item.enabled) {
                    return (
                      <Link
                        className={`nav-link bg-white border-0 sub-answer ${
                          location.pathname.includes(item.link)
                            ? "nav-link-active"
                            : ""
                        }`}
                        to={`${item.link}`}
                        key={"sidenav" + i}>
                        {/* <div className="sb-nav-link-icon">
                          <i
                            className={`fa ${item.icon}`}
                            aria-hidden="true"></i>
                        </div> */}
                        {item.label}
                      </Link>
                    );
                  } else {
                    return (
                      <div
                        className={`nav-link sidenav-disabled sub-answer`}
                        key={"sidenav" + i}>
                        {/* <div className="sb-nav-link-icon">
                          <i
                            className={`fa ${item.icon}`}
                            aria-hidden="true"></i>
                        </div> */}
                        {item.label}
                      </div>
                    );
                  }
                })
              : null}
          </div>
        </div>
      </nav>
    </div>
  );

  // return (
  //             <div className="left-sidebar-wraper">
  //               <div className="profile-wrap">
  //                   <h4></h4>
  //               </div>
  //               <div className="profile-details">
  //                   <div className="sub-answer"><Link style={{textDecoration: 'none',color:'#000'}} to="/department">Department</Link></div>
  //                   <div className="sub-answer"><Link style={{textDecoration: 'none',color:'#000'}} to="/service">Service</Link></div>
  //                   <div className="sub-answer"><Link style={{textDecoration: 'none',color:'#000'}} to="/features">Features</Link></div>
  //                   <div className="sub-answer"><Link style={{textDecoration: 'none',color:'#000'}} to="/roles">Roles</Link></div>
  //                   <div className="sub-answer"><Link style={{textDecoration: 'none',color:'#000'}} to="/hospital">Hospital</Link></div>
  //                   <div className="sub-answer"><Link style={{textDecoration: 'none',color:'#000'}} to="/profile">Profile</Link></div>
  //               </div>
  //           </div>
  // )
};

export default AdminSideNav;
