import React from "react";
import { Link } from "react-router-dom";
import { useAppDispatch } from "../../../store/hooks";
import { logout } from "../../../store/slices/user";
import dummyUserImg from "../../../assets/img/default-user.png";

const AdminHeader: React.FC = () => {
  const dispatch = useAppDispatch();
  const toggleLogout = (e: any) => {
    document.getElementById("admin-logout-dropdown")?.classList.remove("show");
  };

  return (
    <nav className="sb-topnav navbar navbar-expand navbar-light bg-white c__nav">
      <Link className="navbar-brand" to="/">
        <img src={""} alt="Logo" />
      </Link>
      <button
        className="btn btn-link btn-sm order-1 order-lg-0 hmr_btn border-0"
        style={{ background: "transparent" }}
        id="sidebarToggle"
        onClick={(e) => {
          e.preventDefault();
          document.body.classList.toggle("sb-sidenav-toggled");
        }}>
        <i className="fa fa-bars"></i>
      </button>

      <ul className="navbar-nav ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <li className="nav-item nan-link-profile dropdown">
          <a
            className="nav-link dropdown-toggle"
            id="userDropdown"
            href="javaScript:void(0);"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
            onClick={(e) => {
              e.preventDefault();
              document
                .getElementById("admin-logout-dropdown")
                ?.classList.toggle("show");
              document.body.addEventListener("click", toggleLogout, {
                capture: true,
                once: true,
              });
            }}>
            <img src={dummyUserImg} className="rounded-circle" alt="user" />
          </a>
          <div
            className="dropdown-menu dropdown-menu-right"
            id="admin-logout-dropdown"
            aria-labelledby="userDropdown">
            <Link className="dropdown-item" to={`/settings`}>
              Change Password
            </Link>
            <div className="dropdown-divider"></div>
            <a
              className="dropdown-item"
              href="javaScript:void(0);"
              onClick={(e) => {
                e.preventDefault();
                dispatch(logout());
                // history.push(`/login`);
              }}>
              Logout
            </a>
          </div>
        </li>
      </ul>
    </nav>
  );
};

export default AdminHeader;
